﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

using HtmlAgilityPack;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MissionPossible {
    class Movie {
        public string Title { get; set; }
        public string OrigTitle { get; set; }
        public int Year { get; set; }
        public List<string> Actors { get; set; }
        public Uri Location { get; set; }
        public int Rating { get; set; }
        public int Distance { get; set; }
    }

    class Program {
        private static log4net.ILog log;

        private const string CC_BASE = "http://www.cinecartaz.publico.pt";
        private const string RT_BASE = "https://www.rottentomatoes.com";
        private const string CHROME_USER_AGENT
            = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36";

        private static HtmlWeb web = null;

        static Program()
        {
            var logRepository = log4net.LogManager.GetRepository(Assembly.GetEntryAssembly());
            log4net.Config.XmlConfigurator.Configure(logRepository, new FileInfo("App1.config"));
            log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        }

        static void Main(string[] args) {

            web = new HtmlWeb();
            web.UserAgent = CHROME_USER_AGENT;

            var htmlDoc = web.Load(CC_BASE);
            var nodes = htmlDoc.DocumentNode.SelectNodes("//div[@id = 'lista-filmes']/div/ul/li/a");
            var movieMatches = new List<Tuple<Movie, Movie>>();
            foreach (var node in nodes) {
                Movie ccMovie = new Movie();
                ccMovie.Location = new Uri(CC_BASE + node.Attributes["href"].Value);
                ccMovie.Title = node.Attributes["title"].Value;
                FillCCDetails(ccMovie);
                log.Info("CineCartaz - " + JsonConvert.SerializeObject(ccMovie, Formatting.Indented));
                if (ccMovie.OrigTitle == null) {
                    log.Warn($"Skipping movie \"{ccMovie.Title}\"");
                    continue;
                }

                Movie rtMovie = FillRTDetails(ccMovie);
                log.Info("Selected - " + JsonConvert.SerializeObject(rtMovie, Formatting.Indented));
                if (rtMovie?.Distance > 10) {
                    log.Error("Wrong movie!");
                    rtMovie = null;
                } else if (rtMovie?.Distance > 5) {
                    log.Warn("Possibe mismatch!");
                }
                movieMatches.Add(new Tuple<Movie, Movie>(ccMovie, rtMovie));
            }
            using (StreamWriter sw = new StreamWriter("OvosPodres.md")) {
                sw.WriteLine("Título|Ano|RT");
                sw.WriteLine("------|---|--");
                foreach (var moviePair in movieMatches.OrderByDescending(mt => mt.Item2?.Rating)) {
                    StringBuilder sb = new StringBuilder();
                    sb.Append(moviePair.Item1.Title);
                    sb.Append('|');
                    sb.Append(moviePair.Item1.Year);
                    sb.Append('|');
                    if (moviePair.Item2 == null) {
                        sb.Append('-');
                    } else {
                        sb.Append('[');
                        if (moviePair.Item2.Rating == -1) {
                            sb.Append('-');
                        } else {
                            sb.Append(moviePair.Item2.Rating);
                        }
                        sb.Append("](");
                        sb.Append(moviePair.Item2.Location);
                        sb.Append(')');
                    }
                    sw.WriteLine(sb.ToString());
                }
            }
        }

        public static void FillCCDetails(Movie movie) {
            var htmlDoc = web.Load(movie.Location);
            string technicalDetails = "//div[@id = 'content']/div/div/div/div/dl";
            var keys = htmlDoc.DocumentNode.SelectNodes(technicalDetails + "/dt");
            if (keys == null) return;
            var values = htmlDoc.DocumentNode.SelectNodes(technicalDetails + "/dd");

            for (int i = 0; i < keys.Count; ++i) {
                string key = keys[i].InnerText;
                string value = values[i].InnerText;

                switch (key) {
                    case "Título original:":
                        movie.OrigTitle = value;
                        break;
                    case "Com:":
                        movie.Actors = value                                                                          // "Ariane Ascaride (Voz), Jean-Pierre Darroussin, Gérard Meylan, Jacques Boudet"
                            .Split(',')                                                                               // { "Ariane Ascaride (Voz)", " Jean-Pierre Darroussin", " Gérard Meylan", " Jacques Boudet" }
                            .Select(actor => actor.Trim())                                                            // { "Ariane Ascaride (Voz)", "Jean-Pierre Darroussin", "Gérard Meylan", " Jacques Boudet" }
                            .Select(actor => RemoveDiacritics(actor))                                                 // { "Ariane Ascaride (Voz)", "Jean-Pierre Darroussin", "Gerard Meylan", " Jacques Boudet" }
                            .Select(actor => actor.ToLower())                                                         // { "ariane ascaride (voz)", "jean-pierre darroussin", "gerard meylan", " jacques boudet" }
                            .Select(actor => actor.EndsWith(" (voz)") ? actor.Substring(0, actor.Length - 6) : actor) // { "ariane ascaride", "jean-pierre darroussin", "gerard meylan", " jacques boudet" }
                            .ToList();
                        break;
                    case "Outros dados:":
                        string strYear = value                                        // "FRA, 2017, Cores, 107 min."
                            .Split(',')                                               // { "FRA", " 2017", " Cores", " 107 min."}
                            .Select(part => part.Trim())                              // { "FRA", "2017", "Cores", "107 min."}
                            .SingleOrDefault(part => part.All(c => Char.IsDigit(c))); // "2017"
                        movie.Year = Convert.ToInt32(strYear);                        // 2017
                        break;
                }
            }
        }

        public static Movie FillRTDetails(Movie ccMovie) {
            string simpleCCOrigTitle = RemoveDiacritics(ccMovie.OrigTitle)
                .ToLower()
                .Replace("&#38;", "and"); // &
            string searchableTitle = ccMovie
                .OrigTitle
                .Replace("&#38;", "and") // &
                .Replace("'", "");
            var htmlDocResults = web.Load(RT_BASE + "/search/?search=" + Uri.EscapeDataString(searchableTitle));
            var node = htmlDocResults.DocumentNode.SelectSingleNode("//div[@id = 'main_container']/div/script");
            if (node == null) {
                log.Warn($"Can't find {ccMovie.OrigTitle} in RottenTomatoes");
                return null;
            }
            string scriptSrc = node.InnerHtml;
            int iStart = scriptSrc.IndexOf(", {") + 2;
            int iEnd = scriptSrc.IndexOf("});");
            string jsonStr = scriptSrc.Substring(iStart, iEnd - iStart + 1);
            var json = JObject.Parse(jsonStr);
            var movies = (JArray)json["movies"];
            List<Movie> rtMovies = new List<Movie>();
            foreach (var movie in movies) {
                Movie rtMovie = new Movie();

                rtMovie.Location = new Uri(RT_BASE + (string)movie["url"]);
                string strMeterScore = (string)movie["meterScore"];
                if (strMeterScore != null) {
                    rtMovie.Rating = Convert.ToInt32(strMeterScore);
                } else {
                    rtMovie.Rating = -1;
                }

                // Compare original title
                rtMovie.Title = (string)movie["name"];
                rtMovie.OrigTitle = rtMovie.Title;
                if (rtMovie.Title.EndsWith(')')) {
                    // Possible original title present "Husbands And Lovers (la Villa Del Venerdì)"
                    int iStartOrigName = rtMovie.Title.IndexOf('(') + 1;
                    int iEndOrigName = rtMovie.Title.IndexOf(')') - 1;
                    rtMovie.OrigTitle = rtMovie.Title.Substring(iStartOrigName, iEndOrigName - iStartOrigName + 1);
                    rtMovie.Title = rtMovie.Title.Substring(0, iStartOrigName - 2);
                }
                string simpleRTTitle = RemoveDiacritics(rtMovie.Title).ToLower();
                string simpleRTOrigTitle = RemoveDiacritics(rtMovie.OrigTitle).ToLower();
                rtMovie.Distance = Math.Min(
                    LevenshteinDistance.Compute(simpleCCOrigTitle, simpleRTTitle),
                    LevenshteinDistance.Compute(simpleCCOrigTitle, simpleRTOrigTitle));

                // Compare year
                string strYear = (string)movie["year"];
                if (strYear != null) {
                    rtMovie.Year = Convert.ToInt32(strYear);
                }
                rtMovie.Distance += Math.Abs(ccMovie.Year - rtMovie.Year);

                // Compare actors
                rtMovie.Actors = new List<string>();
                var castItems = (JArray)movie["castItems"];
                if (castItems != null) {
                    foreach (var castItem in castItems) {
                        string castItemName = (string)castItem["name"];
                        rtMovie.Actors.Add(RemoveDiacritics(castItemName).ToLower());
                    }
                }
                if (ccMovie.Actors != null) {
                    foreach (string ccActor in ccMovie.Actors) {
                        foreach (string rtActor in rtMovie.Actors) {
                            if (LevenshteinDistance.Compute(ccActor, rtActor) < 3) {
                                --rtMovie.Distance;
                            }
                        }
                    }
                }

                rtMovies.Add(rtMovie);
                log.Debug("Considering - " + JsonConvert.SerializeObject(rtMovie, Formatting.Indented));
            }

            // Find movie with the minimum distance
            return rtMovies.Aggregate((i1, i2) => i1.Distance < i2.Distance ? i1 : i2);
        }

        // https://stackoverflow.com/a/4418510
        public static string RemoveSpecialCharacters(string input)
        {
            Regex r = new Regex("(?:[^a-z0-9_]|(?<=['\"])s)", RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.Compiled);
            string newStr = r.Replace(input, String.Empty);
            while (newStr.IndexOf("__") != -1) {
                newStr = newStr.Replace("__", "_");
            }
            return newStr;
        }

        // https://stackoverflow.com/a/249126
        public static string RemoveDiacritics(string text)
        {
            var normalizedString = text.Normalize(NormalizationForm.FormD);
            var stringBuilder = new StringBuilder();

            foreach (var c in normalizedString)
            {
                var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
                if (unicodeCategory != UnicodeCategory.NonSpacingMark)
                {
                    stringBuilder.Append(c);
                }
            }

            return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
        }
    }
}
